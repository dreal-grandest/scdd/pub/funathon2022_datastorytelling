---
title: "Funathon - Sujet 8 - Code en R"
format:
  html:
    toc: true
    toc-location: right
    toc-title: "Sommaire"
    code-fold: false
    code-summary: "Voir le code"
    code-copy: true
---
```{r}
#| label: library
#| include: false
#| warning: false
library(tidyverse)
library(sf)
library(tmap)
```

## Chargement de la table occupation en 2018

```{r}
#| label: table_occupation_2018
#| warning: false
#| echo: true

Teruti_2018 = read_csv2('../data/Teruti_2018.csv', skip=8)
Teruti_2018 = Teruti_2018 %>% mutate(code = substr(`///Département`,1,2))

names(Teruti_2018)
```

## Ouverture table géo

```{r}
#| label: table_geo
#| warning: false
#| echo: true

departement = st_read('../data/contour-des-departements.json')

```

## Carte artificialisation

```{r}
#| label: carte_artificialisation
#| fig-cap: "Carte artificialisation 2018"
#| warning: false
#| echo: true
sTeruti_2018 = merge(departement, Teruti_2018, by = 'code')
sTeruti_2018 = sTeruti_2018 %>% mutate(tx_artificialisé = 100 * `Tous sols/Sols artificiels//` / `Tous sols///`)

tm_shape(sTeruti_2018) +
  tm_polygons("tx_artificialisé", style="quantile", title="% sols artificialisés")

```

## Table historique
```{r}
#| label: table_histo
#| warning: false
#| echo: true
Teruti_historique = read_csv2('../data/Teruti_historique.csv', skip=7)

```