Dépôt contenant le résultat des travaux effectués par l'équipe du SCDD de la Dreal Grand Est durant le Funathon 2022 qui s'est déroulé les 20 et 21 juin 2022.

__Déplacé le 6 octobre 2022 dans  https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-grandest/scdd/funathon2022_datastorytelling -> https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-grandest/scdd/pub/funathon2022_datastorytelling__

__A FAIRE: Une fois le feu vert de migration envoyé à la Dnum pour changer l'architecture de dépôt sur la Forge GitLab du MTE : Changer la visibilité Interne --> public__

Le résultat est disponible sous la forme d'un site Web généré à partir de l'outil Quarto étudié ce funathon. Le GItlab Page est disponible à l'emplace suivant: <https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-grandest/scdd/funathon2022_datastorytelling/pages>.

Le GitLab Page est un **site web statique** qui est le résultat du **"Build & du Rendering"**, donc de la **génération des pages hmtl** effectuées à partir des scripts existant. Ici, ces scripts sont en quarto, en R, python, js, ... Le Build et le Rendering peut être fait depuis StudioR. Il est fait également à chaque dépôt dans la branche main sur le GitLab car la **configuration CI/CD** est prévue comme cela: Cf bouton prévue à cet usage sur la page d'accueil. Le fichier de configuration est le ***.gitlab-ci.yml*** que l'on peut retrouver sous la racine de l'entrepôt.

# Sujet 8 Réaliser un site de Data Storytelling sur l’occupation du sol et l’artificialisation

L’objectif du sujet est de construire et de raconter à partir de la base de données Teruti, qui propose une vision historique et territoriale de l’occupation du sol, une histoire sous la forme d’un site web réalisé à l’aide de Quarto.

Un site explicatif est disponible à l'adresse : <https://ssplab.pages.lab.sspcloud.fr/2022-06-funathon/sujet-8-realiser-un-site-de-data-storytelling-sur-l-occupation-du-sol-et-l-artificialisation/>

Vous y trouverez :

-   la présentation du sujet
-   des exemples de pages de dataviz
-   une documentation pour réaliser un site web Quarto
