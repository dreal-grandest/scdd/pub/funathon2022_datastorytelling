---
pagetitle: "Quarto - Funathon 2022 - DREAL GE"
page-layout: custom
section-divs: false
toc: true
toc-location: left
css: index.css
editor: source
---

::: hero-banner
::: {.hero-image .hero-image-left}
![](images/hero_left.png)
:::

::: content-block
# Funathon 2022 - DREAL Grand Est

### Notre histoire de ces deux jours

#### *Ce que nous avons pu faire et ne pas faire ;-)*

Montée en compétence sur 3 sujets :

-   Twitter - changement climatique / législative - API, ElasticSearch
-   Montée des eaux - jointure données raster et chiffrées
-   Notre histoire racontée - Quarto

::: hero-buttons
[Twitter](/sujetdgef2022/sujet3_twt.qmd){.btn-action-primary .btn-action .btn .btn-success .btn-lg role="button"} [Montée des eaux](/sujetdgef2022/sujet6_eau.qmd){.btn-action-primary .btn-action .btn .btn-success .btn-lg role="button"} [Notre Funathon](/fullpagedge/){.btn-action-success .btn-action .btn .btn-success .btn-lg role="button"}
:::
:::

::: {.hero-image .hero-image-right}
![](images/_capt_visio_funathon_2.jpg)
:::
:::
