---
title: "La team DREAL Grand Est - Funathon 2022"
about:
  template: jolla
  links:
    - icon: github
      text: Git DREAL Grand Est
      href: https://gitlab-forge.din.developpement-durable.gouv.fr/pub/dreal-grandest
---

![](images/_capt_visio_funathon_1.jpg)

Les participants : Van, Thierry, Florent, Olivier, Eric et François.

Un grand merci aux organisateurs du Funathon INSEE.

Ce site Quarto a été largement inspiré du modèle fourni par [anael.delorme\@agriculture.gouv.fr](mailto:anael.delorme@agriculture.gouv.fr?subject=%5BQuarto-DataViz%5D)
